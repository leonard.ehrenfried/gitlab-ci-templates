# Gitlab CI templates

This collection of templates will help you to kickstart your build on Gitlab CI.

Initially this repo will focus on enabling dependency caching but other best
practise will be added, too.

Merge requests welcome!
